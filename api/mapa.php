<?php 
	header("Content-Type: application/json");

	$mapaFila1 = array(
		array("x" => 10, "y" => 12),
		array("x" => 12, "y" => 120),
		array("x" => 13, "y" => 12, "bomb" => true)
	);

	$mapaFila2 = array(
		array("x" => 10, "y" => 11),
		array("x" => 12, "y" => 12, "bomb" => true),
		array("x" => 13, "y" => 13, "bomb" => true)
	);

	$mapa = array(
		"mapa" 	=> array($mapaFila1, $mapaFila2),
		"score"	=> 10,
		"reset" => false,
		"tryAgain" => false,
		"user" => array(
			"name" => "Alex",
			"nick" => "lexcasa"
		)

	);

	echo json_encode($mapa);	
?>