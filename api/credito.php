<?php 
	header("Content-Type: application/json");



	$personas = array(
		array(
			"cliente" => array(
							"nombre" => "Alex", 
							"cedula" => "123213",
							"edad" => 28,
							"ingresos" => "20100"
						),
			"habilitado" => true,
			"disponible" => "123123"
		),	
		array(
				"cliente" => array(
								"nombre" => "Juan", 
								"cedula" => "12",
								"edad" => 50,
								"ingresos" => "19000"
							),
				"habilitado" => false,
				"disponible" => "123123"
		),	
		array(
				"cliente" => array(
								"nombre" => "Pedro", 
								"cedula" => "22222",
								"edad" => 53,
								"ingresos" => "20200"
							),
				"habilitado" => true,
				"disponible" => "123123"
		)	
	);

	$edad 		= $_GET["edad"];
	$ingresos	= $_GET["ingresos"];
	$habilitado = true;

	$arrHabilitados = array();
	
	for ($i=0; $i < count($personas) ; $i++) { 
		$cliente1 = $personas[$i];
		$cond 	  = ($cliente1["cliente"]["edad"] >= $edad) && 
					($cliente1["cliente"]["ingresos"] >= $ingresos) && 
					($cliente1["habilitado"] == $habilitado);
		
		if($cond == true){
			$arrHabilitados[] = $cliente1;
		}
	}

	$res = array(
			"error" => array(
				"mensaje" => "No existe clientes habilitados"
			)
	);

	if($arrHabilitados){
		$res = array(
			"response" => array(
				"clientesHabilitados" => $arrHabilitados
			)
		);
	}
	

	echo json_encode($res);
?>