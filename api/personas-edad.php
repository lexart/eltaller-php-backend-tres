<?php 
	header("Content-Type: application/json");

	$personas = array(
		array("nombre" => "Alex", "edad" => 28),
		array("nombre" => "Juan", "edad" => 30),
		array("nombre" => "Pepe", "edad" => 45),
		array("nombre" => "Marcos", "edad" => 52)
	);
	
	// CAPTURAMOS DE LAS VARIABLES DE URL 
	// URL/?edad=10
	$edad = $_GET["edad"];

	// TIENE DOS PARAMETROS DE ENTRADA: EDAD, ARRAY DE PERSONAS
	// SALIDA ESPERADA: UNA PERSONA (ARRAY)
	function buscaPersona($edadPersona, $arrPersonas){
		// RECORRE ARRAY DE PERSONAS
		for ($i=0; $i < count($arrPersonas) ; $i++) { 
			// ENCUENTRA LA PERSONA CON LA MISMA EDAD DEL GET
			if($edadPersona == $arrPersonas[$i]["edad"]){
				// DEVUELVE PERSONA ENCONTRADA
				return $arrPersonas[$i];
			}
		}
	}
	// A LA VARIABLE PERSONA (ARRAY)
	// EVALUO $edad (VAR URL) y $personas (ARRAY)
	$persona = buscaPersona($edad, $personas);

	$res 	 = array(
					"error" => array(
									"mensaje" => "Error no se encontro persona.")
				);

	if($persona){
		$res = array(
				"response" => array(
					"persona" => $persona
				)
		);
	}

	echo json_encode( $res );
?>