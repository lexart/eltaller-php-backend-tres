<?php 
	header("Content-Type: application/json");
	

	$productos = array(
		"productos" => array(
			array(
				"nombre" => "Tomate",
				"stock"  => true,
				"precio" => 10,
				"categoria" => array(
					"nombre" => "Verduras",
					"subcategorias" => array(
						array(
							"nombre" => "leguminosos"
						)
					)
				),
				"taxonomia" => array(
					"nombre" => "Frutas"
				),
				"moneda" => array(
					"nombre" 	=> "pesos",
					"simbolo"	=> "\$"
				)
			)
		)
	);



	echo json_encode($productos);
?>