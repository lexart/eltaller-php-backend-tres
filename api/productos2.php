<?php 
	header("Content-Type: application/json");

	$productos = array(
		array(
			"producto" => array(
				"nombre" => "SOJA",
				"precio" => 1000,
				"unidad" => "kg"
			),
			"stock" => array(
				"sku" 		=> "ABAB",
				"cantidad"  => 12
			),
			"track" => array(
				array(
					"fecha" 		=> "2020-01-23",
					"contenedor"	=> "ABA/123/1"
				),
				array(
					"fecha" 		=> "2020-01-22",
					"contenedor"	=> "ABA/123/2"
				)
			)
		),
		array(
			"producto" => array(
				"nombre" => "LANA",
				"precio" => 1123,
				"unidad" => "kg"
			)
		)
	);


	echo json_encode($productos);
?>