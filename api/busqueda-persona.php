<?php 
	header("Content-Type: application/json");

	$cod = $_GET["cod"];

	$personas = array( 
		array(
			"cod" 	 => "ABC123",
			"nombre" => "Pedro Casadevall",
			"cargo"  => "UI Designer",
			"bio" 	 => "Hola mundo ...",
			"img" 	 => "https://www.eharmony.co.uk/dating-advice/wp-content/uploads/2011/04/profilephotos-960x640.jpg",
			"infoExtra" => array("Hola", "buenas", "5")
		),
		array(
			"cod" 	 => "AAA123",
			"nombre" => "Juan Correa",
			"cargo"  => "Developer",
			"bio" 	 => "Hola 333",
			"img" 	 => "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVNNGe0DgfYOrZ67OeZYOw587MKkICe3gw3ArXgnW0hRDCndsWJw&s",
			"infoExtra" => array("1", "2", "5")
		),
		array(
			"cod" 	 => "CC1212",
			"nombre" => "Julian Correa",
			"cargo"  => "UI/UX",
			"bio" 	 => "Hola Hola!",
			"img" 	 => "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT0KU1ofTxNR5qICymitub5FkrgPGZgUB8VB9XBuGa8cGcgOESwrA&s",
			"infoExtra" => array("Hola", "2", "5")
		)
	);

	$res = array("error" => "No existe usuario para el siguiente COD: " . $cod);

	for ($i=0; $i < count($personas) ; $i++) { 
		if($personas[$i]["cod"] == $cod){
			$res = array("response" => $personas[$i]);
		}
	}
	echo json_encode($res);
?>