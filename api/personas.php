<?php 
	header("Content-Type: application/json");
	
	$nombre  	= $_GET["nombre"];
	$apellido 	= $_GET["apellido"];

	$persona = array(
			"nombre" 	=> $nombre,
			"apellido"	=> $apellido,
			"hijos"		=> array(
					array(
						"nombre" 	=> "Juan",
						"apellido"	=> "Correa"
					),
					array(
						"nombre" 	=> "Pedro",
						"apellido"	=> "Correa"
					)
				)
	);

	$jsonPersona = json_encode($persona);
	echo $jsonPersona;
?>