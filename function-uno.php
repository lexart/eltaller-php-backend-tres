<?php 

	// FUNCION CON RETORNO
	function calcAreaCuadrado($lado) {
		$areaCuadrado = $lado * $lado;
		return $areaCuadrado;
	}

	$areaCuadrado = calcAreaCuadrado(150);

	echo "Resultado: ";
	echo "<br>";
	echo $areaCuadrado;
?>