<?php 
	$cond1 = (10 > 15);
	$cond2 = ("10" == 10);
	$cond3 = ("10" === 10);
	$cond4 = (13 < 12);
	$cond5 = ("a" == "A");
	$cond6 = ("b" == "c");

	$res1  = $cond1 && $cond6;
	$res2  = $res1 || $cond6;

	echo "Res 1";
	echo $res1;
	
	echo "<br>";

	echo "Res 2";
	echo $res2;

	if ($cond1) {
		echo "INST 1";
	} else {
		echo "INST 2";
	}
?>