<?php 
	$tipoArea = "CIRCULO";

	// FUNCION CON RETORNO
	function calcAreas($lado,$ancho,$tipo) {
		$resultado = 0;
		if($tipo == "CUADRADO"){
			$resultado = $lado * $lado;
		}

		if($tipo == "RECTANGULO"){
			$resultado = $lado * $ancho;
		}

		if($tipo == "CIRCULO"){
			$pi = 3.1415;
			$resultado = ($lado * $lado) * $pi;
		}
		return $resultado;
	}

	$area = calcAreas(150, 0, $tipoArea);

	echo "Resultado: ";
	echo $tipoArea;
	echo "<br>";
	echo $area;
?>