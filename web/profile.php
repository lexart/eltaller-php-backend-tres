<?php 
	$profile = array();
	$url 	 = "http://localhost/app-php/api/";
	$cod 	 = $_GET["cod"];

	$json 	= json_decode( file_get_contents($url . "busqueda-persona.php?cod=" . $cod), true );
	$error 	= "";

	if(isset($json["response"]) && $json["response"]){
		$profile = $json["response"];
	} else {
		$error = $json["error"];
	}
?>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/profile.css">
</head>
<body>
	<?php if($error == ""){ ?>
		<div class="profile--container">
			<div class="row--div">
				<div class="row-img--container" style='background-image: url(<?php echo $profile["img"]; ?>)'>
					<div class="row-label--container">
						<p><?php echo $profile["nombre"]; ?></p>
						<p style="font-size: 0.6rem"><?php echo $profile["cargo"]; ?></p>
					</div>
				</div>
				
			</div>
			<div class="row--div">
				<div class="row-desc--container">
					<p><?php echo $profile["bio"]; ?></p>
				</div>
				<div class="row-col--container">
					<?php for ($i=0; $i < count($profile["infoExtra"]) ; $i++) { ?>
						<div class="flex--3">
							<?php echo $profile["infoExtra"][$i]; ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php } else { ?>
		<p><?php echo $error; ?></p>
	<?php } ?>
</body>
</html>