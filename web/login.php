<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<form class="formulario">
		<p>Sign in</p>
		<label>Email address</label>
		<br>
		<input class="form--field" type="email" placeholder="Enter email">
		<br>
		<label>Password</label>
		<br>
		<input class="form--field" type="password" placeholder="Enter password">
		<div class="table--div">
			<div class="row--div">
				<div class="cell--div">
					<input type="checkbox" class="form--check">
				</div>
				<div class="cell--div">
					<label>Remember me</label>
				</div>
			</div>
		</div>
		<button>Submit</button>
	</form>
</body>
</html>