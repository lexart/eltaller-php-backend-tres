<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/blog.css">
</head>
<body>
	<div class="grand--container">
		<div class="tab--container">
			<div class="tab--active">Featured Article</div>
			<div>Collections</div>
			<div>Most recommended</div>
		</div>
		<div class="blog--container">
			<div class="blog--row">
				<div class="blog--sample">
					<div class="blog--body">
						<h2>Titulo 1</h2>
						<small>31 Jan 2020</small>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						</p>
					</div>
					<div class="blog--autor">
						<div class="blog--profile">
							<img src="https://image.freepik.com/free-vector/businessman-profile-cartoon_18591-58479.jpg" width="32">
						</div>
						<div class="blog--autor-name">
							<h4>Autor nombre</h4>
							<p>UI Designer</p>
						</div>
						<div class="blog--upvote">
							<span>123</span>
						</div>
					</div>
				</div>
				<div class="blog--img">
					<div class="blog--body-img">
						<div class="blog--overlay"></div>
						<div class="blog--img-content">
							<h4>Titulo 123</h4>
							<small>31 Jan 2020</small>
						</div>
					</div>
					<div class="blog--autor">
						<div class="blog--profile">
							<img src="">
						</div>
						<div class="blog--autor-name">
							<h4>Autor nombre</h4>
							<p>UI Designer</p>
						</div>
						<div class="blog--upvote">
							<span>123</span>
						</div>
					</div>
				</div>
				<div class="blog--sample">
					<div class="blog--body">
						<h2>Titulo 3</h2>
						<small>31 Jan 2020</small>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						</p>
					</div>
					<div class="blog--autor">
						<div class="blog--profile">
							<img src="">
						</div>
						<div class="blog--autor-name">
							<h4>Autor nombre</h4>
							<p>UI Designer</p>
						</div>
						<div class="blog--upvote">
							<span>123</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>